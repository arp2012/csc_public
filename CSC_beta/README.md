CRISPR Specificity Correction (CSC)

This package currently requires python 2.7 to install and run. Future updates of this package will make it compatible with python >3.0.

This package requires scikit-learn==0.16.1 and sklearn-contrib-py-earth 0.1.0

To install system wide, run 

    :::system
        python setup.py build
        python setup.py install

For local installation, run something like

    :::system
        python setup.py install --user

and then make sure that the local directory with binaries (such as `$HOME/Library/Python/2.7/bin/`) is available in your PATH.

To run CSC from the command line, call the entrypoint function

    :::system
        csc_process -h

CSC can take multiple input file formats. CSC reads in a file with the expected format where the first column contains the 20mer gRNA sequences and the second column is a numerical metric (ie: logFC or other output metric). The CSC is able to correct logFC of gRNAs both for pre-computed human genome-wide libraries (Avana, Brunello, GeckoV1, GeckoV2, TKOV3) and custom libraries even if they do not have a pre-computed file. 

If the user utilizes as input a file with gRNA and logFC and the gRNA belongs to a pre-computed library then logFC adjusted for off-target effects will be computed with the following command

    :::system
        csc_process -i filepath-to-input-file -l name-of-precomputed-library

A concrete example of this functionality is available in the /csc_v2/screen_models/examples/

    :::system
        csc_process -i absolute-filepath-to/csc_v2/screen_models/examples/avana_patched_sample_gRNA_lognorm_lnfc.csv -l example

If the user utilizes a file containing only gRNA sequences and the gRNA belongs to a pre-computed library then the off-target space up to a Hamming distance of 3 and the GuideScan specificity score are rendered for each gRNA

    :::system
        csc_process -i absolute-filepath-to/csc_v2/screen_models/examples/avana_patched_sample_gRNA.csv -l example

If a user desires to make their own custom CSC correction for an individual library then they may utilize CSC hg38 (and soon mm10) genome-wide pickle files. Genome wide pickles are located in the following repository.

https://drive.google.com/drive/folders/1U9H3r_CEOpa-MULLCN3awwOdOqG4101T?usp=sharing

    :::system
        csc_process -i absolute-filepath-to-input-file -g absolute-filepath-to-genome-wide-pickle

If the custom file contains both gRNA sequences and numerical metric (ie: logFC or other output metric) values in the first two columns then CSC adjustments will be modeled and made. If only gRNA sequences are present then the output will be the off-target space up to a Hamming distance of 3 and the GuideScan specificity score for each gRNA

To uninstall CSC execute the following command
   
    :::system
        pip uninstall csc
